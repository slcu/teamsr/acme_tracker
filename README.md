## `acme-tracker` - track cells in time-lapse imaging

Package originally authored by [Pierre Barbier de Reuille](https://github.com/PierreBdR).

### Setup

`acme-tracker 0.1` is a `python2` package.
The easiest way to install it and its dependencies is by using `conda`.

If these instructions don't work for you, please [open an issue](https://gitlab.com/tavareshugo/acme_tracker/issues) to let us know.

#### Installing dependencies 

**MacOS:**

1. Open your terminal: press "Cmd + Space", search for and open "Terminal".
1. Ensure you have _Anaconda_/_Miniconda_ installed:
    - On your terminal type `conda --version`. If you get an output move to step 3, if you get an error...
    - ... Download Miniconda3: `curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh`.
    - Run `bash Miniconda3-latest-MacOSX-x86_64.sh` and say "yes" to all questions during installation.
1. Ensure you have _Git_ installed:
    - On your terminal window type `git --version`. If you get an output you're done! Otherwise...
    - ... On your terminal type `conda install git` and when asked say 'y' to proceed with the installation.

**Windows:**

At the moment, to run this aplication on Windows 10, it's best to use the "Linux Subsystem":

1. Install the "Ubuntu Subsystem":
   1. Open "Microsoft Store" (from your Windows menu search for "Store").
   2. Search for "Ubuntu" and install it.
   3. Launch the "Ubuntu" subsystem app.
2. Set up your linux subsystem:
   1. When asked for `Enter new UNIX username:` enter a username of your choice, without including spaces or special characters (this can be the same or a different username as the one you have on Windows itself).
   2. When asked to enter a password create a password of your choice (again, it doesn't have to be the same as the one you use to login to Windows itself).
3. Download and install [Xming](https://sourceforge.net/projects/xming/) accepting all defaults.
4. On your ubuntu terminal run `printf "\n\n# Display graphical apps on Xming\nexport DISPLAY=:0" >> ~/.bashrc`.
5. Close your terminal and open it again.
6. Finally, follow the instructions shown on the **Linux** section below.

**Linux:**

1. Open your terminal.
1. Ensure you have _Anaconda_/_Miniconda_ installed:
    - On your terminal type `conda --version`. If you get an output skip to the next step, if you get an error...
    - ... Download Miniconda3: `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`.
    - Run `bash Miniconda3-latest-Linux-x86_64.sh` and say "yes" to all questions during installation.
1. Ensure you have _Git_ installed (you most likely do!):
    - On your terminal window type `git --version`. If you get an output you're done! Otherwise...
    - On your terminal type `conda install git` and when asked say 'y' to proceed with the installation.


#### Installing `acme_tracker`

Open a terminal:

- **Windows:** From the Start menu, search for and open "Ubuntu" to open your linux terminal.
- **MacOS:** Press "Cmd + Space", search for and open "Terminal".
- **Linux:** Press "Ctrl + Alt + t".

Download and install `acme-tracker` by typing these commands:

```bash
# Clone the package repository
git clone https://gitlab.com/slcu/teamsr/acme-tracker.git

# change to package's directory
cd acme_tracker

# create conda environment
conda env create -f environment.yml

# activate the new environment
conda activate acme_tracker

# install acme_tracker
python setup.py install; python setup.py clean

# (optional) remove the installer
cd ../; rm -rf acme_tracker  ## MAC/Linux
```

### Quick Start

To use `acme_tracker` you need to have the following: 

- a series of images in `.png` format
- a `parameters.ini` file in the same directory

To use `acme_tracker`:

- Open your terminal:
  - **Windows:** From the Windows menu, search for and open "Ubuntu" to open your linux terminal. Also search for and launch "Xming". 
  - **MacOS:** Press "Cmd + Space", search for and open "Terminal".
  - **Linux:** Press "Ctrl + Alt + t".
- Activate your conda environment: `conda activate acme_tracker`.
- `cd` to the directory where your images are located:
  - **Windows:** on the Linux subsystem your _C:_ drive is located in `/mnt/c/`. For example, to navigate to your _Desktop_: `cd /mnt/c/Users/Your.Username/Desktop`.
- Run `acme_tracker`

TODO - provide some test images and parameters file.




<!--
Then, from a terminal, proceed with creating a new conda environment with all necessary dependencies:

```bash
# Create a new environment
conda create -n acme-tracker python=2.7

# Install dependencies
conda install -n acme-tracker pyqt=4 future=0.15 cython imageio path.py scipy configparser
```


```bash
# Create a new environment
conda create -n acme-tracker python=2.7

# Install dependencies
conda install -n acme-tracker pyqt=4 future=0.15 cython imageio path.py scipy configparser
```

Then, proceed with installation of `acme-tracker`:

```bash
# Activate the new conda environment
conda activate acme-tracker

# Clone the package repository
git clone https://gitlab.com/slcu/teamsr/acme-tracker.git

# change to package's directory
cd acme-tracker/acme-tracker-0.1

# Install
python setup.py install

# remove installation folder
rm -r acme-tracker
```
-->

## NEWS

### Version 0.1.1:

- Changes that affect user experience: none
- Internal changes:
  - Adjustments to the `setup.py` file, to remove compilation of QT4 (now recommended to be installed via `conda`) and add a `clean` command to remove compilation folders.
