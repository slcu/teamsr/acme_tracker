#!/usr/bin/env python
from __future__ import print_function
from setuptools import setup, Extension, Command
from Cython.Build import cythonize
from setuptools.command.build_py import build_py
from distutils import log, spawn
import os
import os.path
import sys
import platform
try:
    import PyQt4
    pyqt4_path = os.path.split(PyQt4.__file__)[0]
except ImportError:
    print("You need to install PyQt4.", file=sys.stderr)
import shutil

class build_pyqt4(build_py):
    user_options = build_py.user_options + [
                   ('rcc=', 'r', 'Program used to compile Qt resource files into python module'),
                   ('uic=', 'u', 'Program used to compile Qt UI files into python module')
                   ]

    def initialize_options(self):
        build_py.initialize_options(self)
        self.rcc = None
        self.uic = None

    def finalize_options(self):
        build_py.finalize_options(self)
        self.ensure_filename('rcc')
        self.ensure_filename('uic')

        if platform.system() == 'Windows':
          if self.rcc is None:
            self.rcc = os.path.join(pyqt4_path, 'pyrcc4')
          if self.uic is None:
            self.uic = os.path.join(pyqt4_path, 'pyuic4.bat')
        else:
          if self.rcc is None:
              self.rcc = 'pyrcc4'
          if self.uic is None:
              self.uic = 'pyuic4'

    def run(self):
        build_py.run(self)
        if not self.dry_run:
            print("files = {}".format(self.data_files))
            for package, src_dir, build_dir, files in self.data_files:
                new_files = []
                for f in files:
                    nf = self.make_module(src_dir, build_dir, f)
                    if nf is not None:
                        new_files.append(nf)
                files.extend(new_files)

    def make_module(self, src, pth, name):
        if name.endswith(".ui"):
            return self.make_ui(src, pth, name)
        elif name.endswith(".qrc"):
            return self.make_rc(src, pth, name)

    def make_ui(self, src, pth, name):
        src_file = os.path.join(src, name)
        new_name = "ui_{0}.py".format(name[:-3])
        dst_file = os.path.join(pth, new_name)
        cmd = [self.uic, '--from-imports', '-o', dst_file, src_file]
        log.info('Compile UI file "{0}" into "{1}"'.format(name, dst_file))
        self.spawn(cmd)
        return new_name

    def make_rc(self, src, pth, name):
        src_file = os.path.join(src, name)
        new_name = "{0}_rc.py".format(name[:-4])
        dst_file = os.path.join(pth, new_name)
        if sys.version_info.major == 2:
            ver = '-py2'
        else:
            ver = '-py3'
        cmd = [self.rcc, ver, '-o', dst_file, src_file]
        log.info('Compile RC file "{0}" into "{1}"'.format(name, dst_file))
        self.spawn(cmd)
        return new_name


# https://github.com/pypa/setuptools/issues/1347#issuecomment-387802255
class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        CLEAN_FILES = './build ./dist ./acme_tracker.egg-info'.split(' ')
        for path in CLEAN_FILES:
            shutil.rmtree(path)

kw = {}
if '--with-flycapture' in sys.argv:
    kw['ext_modules'] = cythonize(Extension(
        "flycapture2",                # the extesion name
        sources=["flycapture2.pyx"],  # the Cython source and
                                      # additional C++ source files
        language="c++",               # generate and compile C++ code
        include_dirs = ["/usr/include/flycapture"],
        libraries = ["flycapture"]
        ))

setup(
    name = "acme-tracker",
    version = "0.1.1",
    author = "Pierre Barbier de Reuille",
    author_email = "pierre.barbierdereuille@gmail.com",
    packages = ['acme_tracker'],
    package_data = {'acme_tracker': ['*.ui', '*.qrc']},
    entry_points = {
        'gui_scripts': ['acme_tracker = acme_tracker.tracker:main']
    },
    cmdclass={'build_py': build_pyqt4, 'clean': CleanCommand},
    **kw
)
