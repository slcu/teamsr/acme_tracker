from __future__ import print_function, absolute_import
from skimage import feature

def find(img, features, search_area, cutoff):
    res = []
    for feat, pos in features:
        start_x = max(0, pos[1] - search_area[1])
        start_y = max(0, pos[0] - search_area[0])
        end_x = min(img.shape[1], pos[1] + feat.shape[1] + search_area[1])
        end_y = min(img.shape[0], pos[0] + feat.shape[0] + search_area[0])
        search_img = img[start_y:end_y,start_x:end_x]
        corr = feature.match_template(search_img, feat)
        if corr.max() < cutoff:
            raise ValueError("Couldn't find feature: max correlation = ", corr.max())
        best_pos = np.unravel_index(corr.argmax(), corr.shape)
        best_pos = (best_pos[0] + start_y, best_pos[1] + start_x)
        best_rect = np.index_exp[best_pos[0]:best_pos[0] + feat.shape[0],
                                 best_pos[1]:best_pos[1] + feat.shape[1]]
        res.append((img[best_rect], best_pos))
    return res
