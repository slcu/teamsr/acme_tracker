from PyQt4 import QtGui, QtCore

class TrackerView(QtGui.QGraphicsView):
    DrawRectMode = 3

    selectedRectangle = QtCore.pyqtSignal(QtCore.QRect)

    def __init__(self, *args):
        QtGui.QGraphicsView.__init__(self, *args)
        self._select_rect = False

    def _update_sel_rect(self):
        left = min(self._start_pos.x(), self._end_pos.x())
        right = max(self._start_pos.x(), self._end_pos.x())
        up = min(self._start_pos.y(), self._end_pos.y())
        down = max(self._start_pos.y(), self._end_pos.y())
        rect = QtCore.QRect(left, up, right-left, down-up)
        self._sel_rect = rect
        self.viewport().update()

    def mousePressEvent(self, event):
        mode = self.dragMode()
        if mode == TrackerView.DrawRectMode:
            self._select_rect = True
            self._start_pos = self.mapToScene(event.pos())
            self._end_pos = self._start_pos
            self._update_sel_rect()
            event.accept()
            return
        QtGui.QGraphicsView.mousePressEvent(self, event)

    def mouseMoveEvent(self, event):
        if self._select_rect:
            rect = self.sceneRect()
            pos = self.mapToScene(event.pos())
            if pos.x() < rect.left():
                pos.setX(rect.left())
            elif pos.x() > rect.right():
                pos.setX(rect.right())
            if pos.y() < rect.top():
                pos.setY(rect.top())
            elif pos.y() > rect.bottom():
                pos.setY(rect.bottom())
            self._end_pos = pos
            self._update_sel_rect()
            event.accept()
            return
        QtGui.QGraphicsView.mouseMoveEvent(self, event)

    def mouseReleaseEvent(self, event):
        if self._select_rect:
            self._select_rect = False
            self._update_sel_rect()
            event.accept()
            self.selectedRectangle.emit(self._sel_rect)
            return
        QtGui.QGraphicsView.mouseReleaseEvent(self, event)

    def drawForeground(self, painter, rect):
        QtGui.QGraphicsView.drawForeground(self, painter, rect)
        if self._select_rect:
            pen = QtGui.QPen(QtGui.QColor(200, 0, 0, 255))
            pen.setWidth(1)
            painter.setPen(pen)
            brush = QtGui.QBrush(QtGui.QColor(200, 200, 200, 80))
            painter.setBrush(brush)
            painter.drawRect(self._sel_rect)
