from __future__ import print_function, absolute_import, division
from .camera import Camera
from .ui_tracker import Ui_Tracker
from .tracker_view import TrackerView
from .tracker_thread import TrackerThread, CameraThread, ndarray2QImage, Feature

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import pyqtSlot
import sys
import numpy as np
from collections import namedtuple
try:
  from itertools import izip as zip
except ImportError:
  pass # Then zip will do.
try:
    from path import path
except ImportError:
    from path import Path as path  # Version 10+

try:
    from backports.configparser import ConfigParser
except ImportError:
    raise ImportError("You need to install the 'future' package")

def showException(exc):
    QtGui.QMessageBox.critical(None, "Exception caught", str(exc))

class FeatureItem(QtGui.QGraphicsRectItem):
    rectColors = [QtGui.QColor(0, 200, 0),
                  QtGui.QColor(0, 0, 200),
                  QtGui.QColor(0, 200, 200),
                  QtGui.QColor(200, 0, 200),
                  QtGui.QColor(200, 200, 0)]

    def __init__(self, rect, num):
        real_rect = QtCore.QRectF(0, 0, rect.width(), rect.height())
        QtGui.QGraphicsRectItem.__init__(self, real_rect)
        self.setPos(rect.x(), rect.y())
        self._num = num
        color_idx = num % len(FeatureItem.rectColors)
        color = FeatureItem.rectColors[color_idx]
        bg_color = QtGui.QColor(color)
        bg_color.setAlpha(64)
        self._color = color
        self.setColor(color)
        self.setFlags(QtGui.QGraphicsItem.ItemIsMovable |
                      QtGui.QGraphicsItem.ItemIsSelectable |
                      QtGui.QGraphicsItem.ItemSendsGeometryChanges)
        self.setAcceptHoverEvents(True)

    def setColor(self, col):
        pen = QtGui.QPen(col)
        pen.setWidth(1)
        self.setPen(pen)
        bg_col = QtGui.QColor(col)
        bg_col.setAlpha(64)
        brush = QtGui.QBrush(bg_col)
        self.setBrush(brush)

    def invalidate(self):
        h, s, v, a = self._color.getHsvF()
        self.setColor(QtGui.QColor.fromHsvF(h, .3, .5))

    def validate(self):
        self.setColor(self._color)

    def hoverEnterEvent(self, event):
        if self.flags() & QtGui.QGraphicsItem.ItemIsMovable:
            self.setColor(self._color.lighter())
        QtGui.QGraphicsRectItem.hoverEnterEvent(self, event)

    def hoverLeaveEvent(self, event):
        if self.flags() & QtGui.QGraphicsItem.ItemIsMovable:
            self.setColor(self._color)
        QtGui.QGraphicsRectItem.hoverLeaveEvent(self, event)

    def itemChange(self, change, value):
        if change == QtGui.QGraphicsItem.ItemPositionChange:
            scene = self.scene()
            pos = value.toPointF()
            xint = int(pos.x())
            yint = int(pos.y())
            x = xint + self.rect().left()
            y = yint + self.rect().top()
            width = self.rect().width()
            height = self.rect().height()
            new_rect = QtCore.QRectF(x, y, width, height)
            scene_rect = scene.sceneRect()
            if scene_rect.contains(new_rect):
                pos.setX(xint)
                pos.setY(yint)
                return QtCore.QVariant(pos)
            dx = 0
            if new_rect.left() < scene_rect.left():
                dx = scene_rect.left() - new_rect.left()
            elif new_rect.right() > scene_rect.right():
                dx = scene_rect.right() - new_rect.right()
            dy = 0
            if new_rect.top() < scene_rect.top():
                dy = scene_rect.top() - new_rect.top()
            elif new_rect.bottom() > scene_rect.bottom():
                dy = scene_rect.bottom() - new_rect.bottom()
            pos.setX(xint+dx)
            pos.setY(yint+dy)
            return QtCore.QVariant(pos)
        return QtGui.QGraphicsRectItem.itemChange(self, change, value)

class TrackerScene(QtGui.QGraphicsScene):

    featureMoved = QtCore.pyqtSignal(int, QtCore.QPoint)

    def __init__(self, *args):
        QtGui.QGraphicsScene.__init__(self, *args)

class TrackerWindow(QtGui.QMainWindow):
    def __init__(self, pth, image_path):
        QtGui.QMainWindow.__init__(self)
        config_file = pth / "parameters.ini"

        # Get configuration
        conf = ConfigParser(inline_comment_prefixes=(';',), strict=False)
        conf.read(config_file)
        self.nbFeatures = int(conf.get("Tracking", "nbFeatures"))
        self.tracking_file = pth / conf.get("Tracking", "output")
        self.search_distance = int(conf.get("Tracking", "searchDistance"))
        self.cutoff = float(conf.get("Tracking", "cutoff"))
        self.image_path = image_path
        self.image_id = 0

        with open(self.tracking_file, "w") as f:
            print("Time," + ",".join("X{0},Y{0},score{0}".format(i) for i in range(self.nbFeatures)), file=f)

        self.imageTimer = QtCore.QTimer(self)
        self.imageTimer.setObjectName("imageTimer")
        self.imageTimer.setInterval(1)
        self.imageTimer.setSingleShot(True)

        self.trackerThread = TrackerThread(self)
        self.trackerThread.setObjectName("trackerThread")

        self.camera = Camera(config_file)

        self.cameraThread = CameraThread(self)
        self.cameraThread.setObjectName("cameraThread")

        print(self.camera)
        try:
            self.camera.start()
            data = self.camera.nextImage()
            if data is not None:
                data, img = ndarray2QImage(data)
                self._data = data
                self._img = img
                scene = TrackerScene(QtCore.QRectF(img.rect()), self)
                scene.setObjectName("scene")
                self._scene = scene
                self._image_item = scene.addPixmap(QtGui.QPixmap.fromImage(img))
        except Exception as exc:
            showException(exc)
            return

        self.ui = Ui_Tracker()
        self.ui.setupUi(self)
        self.action_group = QtGui.QActionGroup(self)
        self.action_group.setExclusive(True)
        self.action_group.addAction(self.ui.actionPan)
        self.action_group.addAction(self.ui.actionZoom_in)
        self.action_group.addAction(self.ui.actionSelect_Feature)
        self.action_group.addAction(self.ui.actionPointer)

        #self.ui.actionStartStop.setEnabled(self.nbFeatures == 0)

        self.features = []
        self.feat_items = []

        self.ui.actionPointer.setChecked(True)
        self.ui.imageView.setScene(scene)

        bar = self.statusBar()
        self.status_text = QtGui.QLabel(self)
        bar.addPermanentWidget(self.status_text)
        self.updateStatus()

    @pyqtSlot()
    def on_actionNew_image_triggered(self):
        self.loadNextImage()

    @property
    def ready(self):
        return len(self.feat_items) == self.nbFeatures

    def updateStatus(self):
        self.status_text.setText("# features: {0}/{1}".format(len(self.feat_items), self.nbFeatures))

    @pyqtSlot(bool)
    def on_actionPan_toggled(self, on):
        if on:
            self.ui.imageView.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)

    @pyqtSlot(bool)
    def on_actionPointer_toggled(self, on):
        if on:
            self.ui.imageView.setCursor(QtCore.Qt.ArrowCursor)
            self.ui.imageView.setDragMode(QtGui.QGraphicsView.NoDrag)

    @pyqtSlot(bool)
    def on_actionSelect_Feature_toggled(self, on):
        if on:
            self.ui.imageView.setCursor(QtCore.Qt.CrossCursor)
            self.ui.imageView.setDragMode(TrackerView.DrawRectMode)

    @pyqtSlot(bool)
    def on_actionZoom_in_toggled(self, on):
        if on:
            self.ui.imageView.setCursor(QtCore.Qt.CrossCursor)
            self.ui.imageView.setDragMode(TrackerView.DrawRectMode)

    @pyqtSlot()
    def on_actionZoom_Fit_triggered(self):
        self.ui.imageView.fitInView(self._image_item.boundingRect(), QtCore.Qt.KeepAspectRatio)

    @pyqtSlot()
    def on_actionZoom_100_triggered(self):
        trans = self.ui.imageView.transform()
        move = trans.map(QtCore.QPoint(0, 0))
        new_trans = QtGui.QTransform()
        new_trans.translate(move.x(), move.y())
        self.ui.imageView.setTransform(new_trans)

    @pyqtSlot()
    def on_actionZoom_out_triggered(self):
        self.ui.imageView.scale(.5, .5)

    @pyqtSlot(bool)
    def on_actionStartStop_toggled(self, on):
        if on:
            if self.ready:
                self.setupTrackerThread()
            self.imageTimer.start()
        else:
            self.imageTimer.stop()
            if self.ready:
                self.ui.actionSelect_Feature.setEnabled(True)
            for item in self.feat_items:
                item.setFlag(QtGui.QGraphicsItem.ItemIsMovable, True)
                item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, True)

    def setupTrackerThread(self):
        if self.ui.actionSelect_Feature.isChecked():
            self.ui.actionPointer.setChecked(True)
        self.ui.actionSelect_Feature.setEnabled(False)
        self.extractFeatures()
        self.trackerThread.features = self.features
        self.trackerThread.cutoff = self.cutoff
        self.trackerThread.search_distance = self.search_distance
        self.startTime = QtCore.QDateTime.currentDateTimeUtc()
        for item in self.feat_items:
            item.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)
            item.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)

    def extractFeatures(self):
        features = []
        image_item = self._image_item
        image = self._data
        for item in self.feat_items:
            p1 = item.rect().topLeft()
            p2 = item.rect().bottomRight()
            p1 = image_item.mapFromScene(item.mapToScene(p1))
            p2 = image_item.mapFromScene(item.mapToScene(p2))
            x0 = int(min(p1.x(), p2.x()))
            x1 = int(max(p1.x(), p2.x()))
            y0 = int(min(p1.y(), p2.y()))
            y1 = int(max(p1.y(), p2.y()))
            print("New feature: {0}x{1}+{2}+{3}".format(x1-x0, y1-y0, x0, y0))
            feat = image[y0:y1, x0:x1]
            features.append(Feature(feat, x0, y0, 1))
        self.features = features

    @pyqtSlot()
    def on_trackerThread_trackingDone(self):
        self.features = self.trackerThread.features
        current_time = QtCore.QDateTime.currentDateTimeUtc();
        data = [str(self.startTime.msecsTo(current_time))]
        for feat, feat_item in zip(self.features, self.feat_items):
            feat_item.setPos(feat.x, feat.y)
            feat_item.validate()
            data.append("{0:d},{1:d},{2:g}".format(int(feat.x), int(feat.y), feat.score))
        with open(self.tracking_file, "a") as f:
            print(",".join(data), file=f)
        if self.ui.actionStartStop.isChecked():
            self.imageTimer.start()

    @pyqtSlot(object, QtGui.QImage)
    def on_cameraThread_imageAvailable(self, data, img):
        if data is not None:
            self._image_item.setPixmap(QtGui.QPixmap.fromImage(img))
            self._img = img
            self._data = data
        if self.ready:
            if self.image_path:
                img_pth = self.image_path / 'camera_{:05d}.png'.format(self.image_id)
                self.image_id += 1
                img.save(img_pth)
            self.trackerThread.image = data
            for item in self.feat_items:
                item.invalidate()
            self.trackerThread.start()
        elif self.ui.actionStartStop.isChecked():
            self.imageTimer.start()

    @pyqtSlot()
    def on_imageTimer_timeout(self):
        if self.camera.started:
            self.cameraThread.start()
            return
        self.ui.actionStartStop.setChecked(False)

    def loadNextImage(self):
        self.cameraThread.start()

    @pyqtSlot(QtCore.QRect)
    def on_imageView_selectedRectangle(self, rect):
        if self.ui.actionSelect_Feature.isChecked():
            scene_rect = FeatureItem(QtCore.QRectF(rect), len(self.feat_items))
            self._scene.addItem(scene_rect)
            self.feat_items.append(scene_rect)
            self.updateStatus()
            if self.ready:
                self.ui.actionSelect_Feature.setEnabled(False)
                if self.ui.actionStartStop.isChecked():
                    self.setupTrackerThread()
        elif self.ui.actionZoom_in.isChecked():
            self.ui.imageView.fitInView(QtCore.QRectF(rect), QtCore.Qt.KeepAspectRatio)

    def closeEvent(self, event):
        self.imageTimer.stop()
        event.accept()

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--save-images", action='store', default=path(), type=path,
                        dest="save_images", help="If a path is given, all the captured images will be saved there")
    parser.add_argument("--list-cameras", action='store_true', default=False,
                        dest="cameras", help="List all available camera drivers")
    parser.add_argument("--camera-options", dest="options", type=str,
                        default='', metavar='CAMERA',
                        help="List the options for a given driver")
    parser.add_argument("path", help="Path containing the ini file", type=path, default='.', nargs='?')
    args = parser.parse_args()
    if args.cameras:
        drivers = Camera.drivers()
        print("\n".join(d.name for d in drivers))
        return
    if args.options:
        driver = Camera.driver(args.options)
        print(driver.list_options())
        return
    if args.path:
        if not args.path.isdir():
            raise ValueError("The path specified as argument is not a folder")
        if not (args.path / "parameters.ini").isfile():
            raise ValueError("The path specified doesn't contain a 'tracker.ini' file")
    else:
        args.path = path(__file__).dirname()

    if args.save_images and not args.save_images.exists():
        args.save_images.makedirs()

    app = QtGui.QApplication(sys.argv)
    gui = TrackerWindow(args.path, args.save_images)
    gui.show()
    return app.exec_()
