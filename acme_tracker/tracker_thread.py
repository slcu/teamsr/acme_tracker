from __future__ import print_function, absolute_import, division
from .template_matching import norm_xcorr

#from skimage import feature
from PyQt4 import QtCore, QtGui
import numpy as np
from collections import namedtuple
from time import clock
# from scipy import misc

Feature = namedtuple("Feature", ["image", "x", "y", "score"])

def ndarray2QImage(data):
    """
    Convert a numpy array into a QImage
    """
    upper = data.max()
    lower = data.min()
    delta = upper - lower
    data = (255 * ((data - lower) / delta))
    data = np.asarray(data, dtype=np.uint8, order='C')
    width, height = data.shape[:2]
    fmt = QtGui.QImage.Format_Indexed8
    if len(data.shape) == 3:
        if data.shape[2] not in (3, 4):
            raise ValueError("The image returned by the camera must have 1, 3 or 4 channels")
        fmt = QtGui.QImage.Format_ARGB32
        if data.shape[2] == 3:
            extra = 255*np.ones(data.shape[:2], dtype=data.dtype)[..., None]
            data = np.concatenate([data, extra], axis=2)
    stride = data.strides[0]
    img = QtGui.QImage(data.data, height, width, stride, fmt)
    if fmt == QtGui.QImage.Format_Indexed8:
        img.setColorCount(256)
        for i in range(256):
            img.setColor(i, QtGui.qRgba(i, i, i, 255))
    return data, img


class TrackerThread(QtCore.QThread):
    trackingDone = QtCore.pyqtSignal()

    def __init__(self, parent):
        QtCore.QThread.__init__(self, parent)
        self.features = None
        self.image = None
        self.search_distance = None
        self.cutoff = None
        self.count = 0

    def run(self):
        new_features = []
        image = self.image / self.image.max()
        search_distance = self.search_distance
        cutoff = self.cutoff
        # self.count += 1
        # filename = "image_{0:05d}.png".format(self.count)
        # misc.imsave(filename, image)
        for i, feat in enumerate(self.features):
            print("Old pos: ", feat.x, feat.y)
            start_x = max(0, feat.x - search_distance)
            start_y = max(0, feat.y - search_distance)
            end_x = min(image.shape[1], feat.x + feat.image.shape[1] + search_distance)
            end_y = min(image.shape[0], feat.y + feat.image.shape[0] + search_distance)
            search_img = image[start_y:end_y, start_x:end_x]
            # filename = "template_{0:05d}_{1:d}.png".format(self.count, i)
            # misc.imsave(filename, feat.image)
            # filename = "search_{0:05d}_{1:d}.png".format(self.count, i)
            # misc.imsave(filename, search_img)
            #corr = feature.match_template(search_img, feat.image)
            corr = norm_xcorr(feat.image / feat.image.max(), search_img, 'fourier')
            score = corr.max()
            new_feat = None
            if score < cutoff:
                print("Failed pos: ", score)
                new_feat = feat._replace(score=score)
            else:
                best_pos = np.unravel_index(corr.argmax(), corr.shape)
                best_x = best_pos[1] + start_x - feat.image.shape[1] // 2
                best_y = best_pos[0] + start_y - feat.image.shape[0] // 2
                print("New pos: ", best_x, best_y, score)
                best_rect = np.index_exp[best_y:best_y + feat.image.shape[0],
                                         best_x:best_x + feat.image.shape[1]]
                # The next line keeps the feature constant
                new_feat = Feature(feat.image, best_x, best_y, score)
                # The next line update the feature with the new image
                #new_feat = Feature(image[best_rect], best_x, best_y, score)
                #new_features.append(Feature(image[best_rect], best_x, best_y))
            new_features.append(new_feat)
        self.features = tuple(new_features)
        self.trackingDone.emit()

class CameraThread(QtCore.QThread):
    imageAvailable = QtCore.pyqtSignal(object, QtGui.QImage)

    def __init__(self, parent):
        QtCore.QThread.__init__(self, parent)
        self.camera = parent.camera

    def run(self):
        data = self.camera.nextImage()
        #data, img = ndarray2QImage(data)
        data, img = ndarray2QImage(data)
        self.imageAvailable.emit(data, img)
