from __future__ import print_function, absolute_import
import time
try:
    from scipy.misc import imread
except ImportError:
    from imageio import imread
import numpy as np
try:
    from path import path
except ImportError:
    from path import Path as path  # Version 10+
try:
    from future.utils import with_metaclass
    from backports.configparser import ConfigParser
except ImportError:
    raise ImportError("You need to install the 'future' package")

class Camera(object):
    _registered_drivers = {}

    def __init__(self, config_file):
        conf = ConfigParser(inline_comment_prefixes=(';',), strict=False)
        conf.read(config_file)
        self.config = conf
        camera_name = conf.get('Tracking', 'camera')
        camera_type = conf.get(camera_name, 'type')
        if camera_type not in Camera._registered_drivers:
            raise ValueError("Unknown camera type '{}'".format(camera_type))
        self._driver = Camera._registered_drivers[camera_type](config_file, camera_name)
        self._camera_type = camera_type
        self._camera_name = camera_name

    @property
    def camera_type(self):
        return self._camera_type

    @property
    def camera_name(self):
        return self._camera_name

    @property
    def started(self):
        return self._driver.started

    def start(self):
        if self.started:
            raise ValueError("Camera acquisition already started")
        self._driver.start()

    def stop(self):
        if not self.started:
            raise ValueError("Camera acquisition not started")
        self._driver.stop()

    def nextImage(self):
        if not self.started:
            raise ValueError("Camera acquisition not started")
        return self._driver.nextImage()

    def __str__(self):
        drv = str(self._driver)
        return "Camera(driver = {})".format(drv)

    @classmethod
    def driver(cls, name):
        return cls._registered_drivers[name]

    @classmethod
    def drivers(cls):
        return [cls._registered_drivers[n] for n in cls._registered_drivers]

class DriverType(type):
    def __new__(metacls, name, bases, dct):
        cls = type.__new__(metacls, name, bases, dct)
        if 'name' in dct:
            Camera._registered_drivers[dct['name']] = cls
        return cls

class BaseDriver(object):
    pass

class Driver(with_metaclass(DriverType, BaseDriver)):
    """
    Need to specify:

    - name : class attribute
    - start : method with no argument
    - stop : method with no argument
    - nextImage : method with no argument
    - started : property
    - list_options : static or class method returning the list of required arguments
    """
    __metaclass__ = DriverType

try:
    import flycapture2
    class FlyCaptureDriver(Driver):
        name = "flycapture2"

        def __init__(self, config_file, name):
            conf = ConfigParser(inline_comment_prefixes=(';',), strict=False)
            conf.read(config_file)
            video_mode = conf.get(name, 'VideoMode')
            self._video_mode = getattr(flycapture2.VideoMode, video_mode)
            framerate = conf.get(name, 'FrameRate')
            self._framerate = getattr(flycapture2.FrameRate, framerate)
            brightness = conf.get(name, 'Brightness')
            self._brightness = float(brightness)
            exposure = conf.get(name, 'Exposure')
            self._exposure = float(exposure)
            shutter = conf.get(name, 'Shutter')
            self._shutter = float(shutter)
            gain = conf.get(name, 'Gain')
            self._gain = float(gain)
            saturation = conf.get(name, 'Saturation')
            self._saturation = float(saturation)
            self._cam = flycapture2.Camera()
            self._cam.connect()
            self._started = False

        def __del__(self):
            self._cam.disconnect()

        def printCamInfo(self):
            camInfo = self._cam.cameraInfo
            print("""*** CAMERA INFORMATION***
    Serial number - {0}
    Camera model - {1}
    Camera vendor - {2}
    Sensor - {3}
    Resolution - {4}
    Firmware version - {5}
    Firmware build time - {6}""".format(camInfo.serialNumber, camInfo.modelName, camInfo.vendorName,
                                        camInfo.sensorInfo, camInfo.sensorResolution, camInfo.firmwareVersion,
                                        camInfo.firmwareBuildTime))

        def start(self):
            import flycapture2
            cam = self._cam
            if not cam.connected:
                raise ValueError("Error, cannot start the capture as the software is not connected to the camera")
            self.printCamInfo()
            cam.videoModeAndFrameRate = (self._video_mode, self._framerate)
            config = flycapture2.FC2Config()
            config.numImageNotifications = 1
            config.grabMode = flycapture2.GrabMode.DROP_FRAMES
            triggerMode = flycapture2.TriggerMode()
            triggerMode.onOff = False
            cam.triggerMode = triggerMode
            triggerDelay = cam.triggerDelay
            triggerDelay.onOff = False
            cam.triggerDelay = triggerDelay
            cam.configuration = config

            prop = cam.getProperty(flycapture2.PropertyType.BRIGHTNESS)
            prop.absValue = self._brightness
            prop.onOff = True
            cam.setProperty(prop)

            prop = cam.getProperty(flycapture2.PropertyType.AUTO_EXPOSURE)
            prop.absValue = self._exposure
            prop.autoManualMode = False
            prop.onOff = True
            cam.setProperty(prop)

            prop = cam.getProperty(flycapture2.PropertyType.SHUTTER)
            prop.absValue = self._shutter
            prop.autoManualMode = False
            prop.onOff = True
            cam.setProperty(prop)

            prop = cam.getProperty(flycapture2.PropertyType.GAIN)
            prop.absValue = self._gain
            prop.autoManualMode = False
            prop.onOff = True
            cam.setProperty(prop)

            prop = cam.getProperty(flycapture2.PropertyType.SATURATION)
            prop.absValue = self._saturation
            prop.autoManualMode = False
            prop.onOff = True
            cam.setProperty(prop)

            cam.startCapture()
            self._started = True

        def stop(self):
            cam = self._cam
            if not cam.connected:
                raise ValueError("Error, cannot stop the capture as the software is not connected to the camera")
            cam.stopCapture()
            self._started = False

        def nextImage(self):
            print("Retrieving buffer")
            image = self._cam.retrieveBuffer()
            print("   buffer retrieved")
            return image.data

        @property
        def started(self):
            return self._started

        def __str__(self):
            return "flycapture2(mode={0}, framerate={1})".format(self._video_mode, self._framerate)

        @classmethod
        def list_options(cls):
            import flycapture2
            video_modes = [n for n in dir(flycapture2.VideoMode)
                           if n.startswith("VIDEOMODE") and not n.endswith("FORCE_32BITS")]
            framerates = [n for n in dir(flycapture2.FrameRate)
                          if n.startswith("FRAMERATE") and not n.endswith("FORCE_32BITS")]
            return """VideoMode = Video mode of the camera. Must be one of:
        {0}
    FrameRate = frame rate of the acquisition. Must be one of:
        {1}""".format(", ".join(video_modes), ", ".join(framerates))
except ImportError:
    pass  # No FlyCapture driver without the module

class DummyCameraDriver(Driver):
    name = "dummy"

    extensions = ('png', 'jpg', 'tif', 'tiff')

    def __init__(self, config_file, name):
        conf = ConfigParser(inline_comment_prefixes=(';',), strict=False)
        conf.read(config_file)
        folder = path(conf.get(name, 'Folder'))
        if not folder.isdir():
            raise ValueError("Path '{}' doesn't lead to a folder".format(folder))
        self._folder = folder
        self._delay = float(conf.get(name, 'Delay'))
        self._images = None

    def start(self):
        self._images = None
        self._index = 0
        for e in DummyCameraDriver.extensions:
            imgs = self._folder.files('*.' + e)
            if imgs:
                self._images = sorted(imgs)
                break
        else:
            raise ValueError("Couldn't find any images in the folder '{}'".format(self._folder))

    def stop(self):
        self._images = None

    @property
    def started(self):
        return self._images is not None

    def nextImage(self):
        if self._index >= len(self._images):
            return
        image_name = self._images[self._index]
        print("Returning image '{}'".format(image_name))
        img = imread(image_name)
        self._index += 1
        time.sleep(self._delay)
        return img

    def __str__(self):
        return "dummy(folder={0}, delay={1})".format(self._folder, self._delay)

    @classmethod
    def list_options(cls):
        return """Folder = absolute path to the folder containing the images
Delay = pause in seconds before sending the next image (this can be a floating point number)"""
